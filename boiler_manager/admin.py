# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from boiler_manager.model.repository.entity import Boiler, Residence, Event

# Register your models here.
admin.site.register(Boiler)
admin.site.register(Residence)
admin.site.register(Event)
