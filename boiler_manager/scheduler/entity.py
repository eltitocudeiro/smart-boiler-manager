# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import csv
import importlib
import os
import datetime
from types import NoneType

from smart_boiler_manager.settings import MEDIA_ROOT
from django.core.cache import cache



class SchedulerResidence(object):
    def __init__(self, residence):
        self.residence = residence
        self._temp = 0
        self._observers = []

    def get_temp(self):
        return self._temp

    def set_temp(self, value):
        self._temp = value
        for callback in self._observers:
            callback(self._temp)

    temperature = property(get_temp, set_temp)

    def bind_to(self, callback):
        self._observers.append(callback)

    def __repr__(self):
        return "Residence in Scheduler[" + str(self.residence.id) + "] - title:" + self.residence.title



class SchedulerEvent(object):
    def __init__(self, event):
        self.event = event
        self.scheduler_residence = SchedulerResidence(event.residence)
        self.user = event.user

        # Load "module.submodule.MyClass"
        BoilerAdapterClass =  getattr(importlib.import_module("boiler_manager.scheduler.adapter"),
                str(self.event.residence.boiler.type))
        # Instantiate the class (pass arguments to the constructor,)
        self.boiler_adapter = BoilerAdapterClass(self.event.residence.boiler,
                 self.scheduler_residence)

        # boiler_type=str(self.residence.boiler.type)
        # if boiler_type=='Mock':
        #     self.boiler_adapter=BoilerAdapterMock(self.residence.boiler, self.residence)
        # else:
        #     self.boiler_adapter=BoilerAdapter(self.residence.boiler, self.residence)
        self.scheduler_residence.bind_to(self.print_log)
        self.scheduler_residence.bind_to(self.boiler_program)
        self.scheduler_residence.bind_to(self.save_results)

    def __repr__(self):
        return "Event in Scheduler[" + str(self.event.id) + "] - Target Temp.:" + str(self.event.target_temp)

    def boiler_program(self, temperature):
        self.boiler_adapter.program_event(temperature, self.event)

    def print_log(self, temperature):
        print("Event in Scheduler[" + str(self.event.id) + "] - Target Temp.:" + str(self.event.target_temp) + " - Current Temp.:" + str(temperature))
        # Log.write("Event[" + str(self.id) + "] - Residence[" + str(self.residence.id) + "] - Temperature[" + str(
        #     temperature) + "]")

    def save_results(self, temperature):
        event_root = os.path.join(MEDIA_ROOT, 'event')
        event_dir = os.path.join(event_root, str(self.event.id))
        if not os.path.exists(event_dir):
            os.makedirs(event_dir)

        event_path = os.path.join(event_dir, 'audit.csv')
        now = datetime.datetime.utcnow().isoformat() + 'Z'
        try:
            data_model = cache.get(str(self.event.id))
            phase = data_model['status']
        except:
            phase = 'sim'
        if not os.path.exists(event_path):
            with open(event_path, 'wb') as csvfile:
                filewriter = csv.writer(csvfile, delimiter=str(','), quotechar=str('|'), quoting=csv.QUOTE_MINIMAL)
                filewriter.writerow(['Time', 'Temperature', 'Phase'])
                filewriter.writerow([now, temperature, phase])
        else:
            with open(event_path, 'a') as csvfile:
                filewriter = csv.writer(csvfile, delimiter=str(','), quotechar=str('|'), quoting=csv.QUOTE_MINIMAL)
                filewriter.writerow([now, temperature, phase])

    def finish(self):
        self.boiler_adapter.turn_off()

    def start(self):
        self.boiler_adapter.turn_on()
