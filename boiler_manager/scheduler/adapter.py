# -*- coding: utf-8 -*-
from __future__ import print_function
import time
from django.core.cache import cache


from boiler_manager.model.service import GoogleMapsService, PiService, OpenWeatherService
from boiler_manager.scheduler import scheduler


class BoilerAdapter(object):

    def __init__(self, boiler, residence):
        self.boiler = boiler
        self.residence = residence
        self.switched_on = False
        self.warming_up = None

    def warm(self, power):
        pass

    def rest(self):
        pass

    def program_event(self, temperature, event):
        pass

    def turn_off(self):
        pass

    def turn_on(self):
        pass

    def __repr__(self):
        return "BoilerAdapter[" + str(self.boiler.id) + "]"


class BoilerAdapterNormal(BoilerAdapter):

    def __init__(self, boiler, residence):
        super(BoilerAdapterNormal, self).__init__(boiler, residence)

    def warm(self, power):
        time.sleep(1)
        PiService.turn_off_RGB()
        time.sleep(1)
        PiService.turn_on_red()

    def rest(self):
        time.sleep(1)
        PiService.turn_off_RGB()
        time.sleep(1)
        PiService.turn_on_blue()

    def program_event(self, temperature, event):
        scheduler.normal_program(self, temperature, event.target_temp - 2, event.target_temp + 2)

    def turn_off(self):
        self.switched_on = False
        time.sleep(10)
        PiService.turn_off_RGB()


    def turn_on(self):
        self.switched_on = True
        scheduler.check_temperature(self)


class BoilerAdapterMock(BoilerAdapter):

    def __init__(self, boiler, residence):
        super(BoilerAdapterMock, self).__init__(boiler, residence)

    def warm(self, power):
        self.warming_up = True
        PiService.turn_off_RGB()
        time.sleep(1)
        PiService.turn_on_red()
        while (self.switched_on and self.warming_up == True):
            time.sleep(1)
            self.residence.temperature += 1
        PiService.turn_off_RGB()

    def rest(self):
        self.warming_up = False
        PiService.turn_off_RGB()
        time.sleep(1)
        PiService.turn_on_blue()
        while (self.switched_on and self.warming_up == False):
            time.sleep(1)
            self.residence.temperature -= 1
        PiService.turn_off_RGB()

    def program_event(self, temperature, event):
        scheduler.sim_program(self, temperature, event.target_temp - 2, event.target_temp + 2)

    def turn_off(self):
        self.switched_on = False

    def turn_on(self):
        PiService.turn_off_RGB()
        PiService.turn_on_green()
        self.switched_on = True
        self.residence.temperature = 273.16 + 15.5


class BoilerAdapterSmart(BoilerAdapter):

    def __init__(self, boiler, residence):
        super(BoilerAdapterSmart, self).__init__(boiler, residence)

    def warm(self, power):
        self.warming_up = True
        # data_model = cache.get{str(event.id)}
        # data_model['status'] = 'p1'
        # cache.set(str(event.id), data_model)

    def rest(self):
        self.warming_up = False

    def program_event(self, temperature, event):
        lat, lon = GoogleMapsService.Geocoding(self.residence.residence.address)
        out_temp = OpenWeatherService.getTemperature(latitud=lat, longitud=lon)
        min_temp_rad = self.boiler.min_temp
        max_temp_rad = self.boiler.max_temp
        data_model = scheduler.smart_program(self, event, temperature, self.residence.residence.x_side, self.residence.residence.y_side,
                               event.target_temp, out_temp, min_temp_rad, max_temp_rad)
        # data_model = scheduler.smart_program(self, event, temperature, 3, 3,
        #                        event.target_temp, out_temp, min_temp_rad, max_temp_rad)
        # if data_model is not None:
        #     data_model['status'] = 'p1'
        #     cache.set(str(event.id), data_model)

        # data_model['status'] = 'p1'
        # cache.set(str(event.id), data_model)


    def turn_off(self):
        self.switched_on = False
        self.warming_up = None

    def turn_on(self):
        self.switched_on = True
        # Interchange the comment of the following two lines in case of implementing a real boiler
        scheduler.check_temperature(self)
        # self.residence.temperature = 273.16 + 15.5


