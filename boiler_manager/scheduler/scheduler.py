# -*- coding: utf-8 -*-
from __future__ import print_function

from threading import Timer
import datetime

import os
from googleapiclient.errors import HttpError
from boiler_manager.model.service import PiService, GoogleCalendarService, EventService, ResidenceService, \
    FirebaseService, GoogleMapsService, LinearRegressionService
from boiler_manager.scheduler.entity import SchedulerEvent, SchedulerResidence
from django.core.cache import cache
import pandas as pd
import csv
import datetime, time



from smart_boiler_manager.settings import MEDIA_ROOT


def beginEvent(scheduler_event):
    Timer(0, scheduler_event.start, ()).start()
    # Timer(3, event.check_temperatura, ()).start()


def finishEvent(scheduler_event):
    Timer(0, scheduler_event.finish, ()).start()
    Timer(60, write_event, (scheduler_event,)).start()


def bootScheduler(event_list):
    cache_event_list = []
    cache_list = {}

    if event_list:
        for entry in event_list:
            print(str(entry) + ':' + str(event_list[str(entry)]['begin_event']))
            print(str(entry) + ':' + str(event_list[str(entry)]['end_event']))
            if not event_list[str(entry)]['event'].boiler_adapter.switched_on:
                event_list[str(entry)]['begin_event'].cancel()
                event_list[str(entry)]['end_event'].cancel()
    else:
        event_list = {}

    residence_list = ResidenceService.get_all()

    for residence in residence_list:
        scheduler_residence = SchedulerResidence(residence=residence)
        calendar = None
        try:
            calendar = GoogleCalendarService.getCalendar(scheduler_residence.residence.google_calendar)
        except:
            print('Residence[' + str(scheduler_residence.residence.id) + ']: Calendar not found: '
                  + str(scheduler_residence.residence.google_calendar))

        if (calendar != None):
            print('Residence[' + str(scheduler_residence.residence.id) + ']: Calendar: '
                  + str(scheduler_residence.residence.google_calendar))
            try:
                calendar_event_list = GoogleCalendarService.getEvents(scheduler_residence.residence.google_calendar, 1)
            except HttpError:
                print(HttpError)

            if not calendar_event_list:
                print('No upcoming event(s) found.')
            else:
                calendar_event = calendar_event_list[0]
                start = calendar_event['start'].get('dateTime', calendar_event['start'].get('date'))
                end = calendar_event['end'].get('dateTime', calendar_event['end'].get('date'))

                begin, begin_delay = GoogleCalendarService.delayTime(start)
                finish, finish_delay = GoogleCalendarService.delayTime(end)

                dt = datetime.datetime.now()
                dt_now = ((dt - datetime.datetime.utcfromtimestamp(0)).total_seconds())

                print('Now: ' + str(dt_now))
                print('Begin: ' + str(begin))
                print('End: ' + str(finish))

                if dt_now < begin:
                    # event = Programa(CasaAdapter(casa), 22, 16)
                    calendar_event_id = calendar_event['id'].rsplit('_', 1)[0]
                    # print str(calendar_event_id)
                    event = EventService.get_by_google_calendar_event(google_calendar_event=calendar_event_id)

                    user = event.user
                    residence = event.residence

                    if residence.boiler.type == 'BoilerAdapterMock':
                        travel_time = 0
                    else:
                        try:
                            lat_user, lng_user = FirebaseService.get_user_position(user.username)
                            user_position = GoogleMapsService.InverseGeocoding(lat_user, lng_user)
                            travel_time = GoogleMapsService.TravelDurationInSeconds(user_position, residence.address)
                        except AttributeError:
                            travel_time = 0

                    if travel_time > begin_delay:
                        real_begin = ((travel_time // 600) * 600)
                    else:
                        real_begin = begin_delay


                    if travel_time < finish_delay:

                        scheduler_event = SchedulerEvent(event)
                        e1 = Timer(real_begin, beginEvent, (scheduler_event,))
                        e2 = Timer(finish_delay, finishEvent, (scheduler_event,))
                        # id = casa.id
                        entry = {}
                        entry['begin'] = begin
                        entry['end'] = end
                        entry['begin_event'] = e1
                        entry['end_event'] = e2
                        entry['event'] = scheduler_event
                        event_list[str(event)] = entry
                        e1.start()
                        e2.start()

                        cache_entry = {
                            'start': start,
                            'end': end,
                            'name': str(event),
                            'id': str(event.id),
                            'left_time': real_begin
                        }
                        cache_event_list.append(cache_entry)

    cache_list['events'] = cache_event_list
    cache.set('cache_list', cache_list)

    Timer(60, bootScheduler, (event_list,)).start()

    return event_list


def smart_program(boiler_adapter, event, temperature, side_x, side_y, target_temp, out_temp, min_temp_rad, max_temp_rad):
    if boiler_adapter.warming_up == True:
        return None
    elif boiler_adapter.warming_up == False:
        return None
    else:
        # temperature+=273.16
        # target_temp+=273.16
        boiler_adapter.warming_up = False
        training_warm = 'simulation_on_heat_200.csv'
        test_warm = 'simulation_on_heat_10.csv'
        training_adjust = 'simulation_off_heat_200.csv'
        test_adjust = 'simulation_off_heat_10.csv'


        # data_model = {}
        # data_model['p1'] = {}
        # data_model['p2'] = {}
        # data_model['p3'] = {}
        # data_model['status'] = 'p1'

        data_model = cache.get(event.id)
        if data_model is None:
            data_model = {}
            data_model['p1'] = {}
            data_model['p2'] = {}
            data_model['p3'] = {}
            data_model['status'] = 'p1'
        cache.set(str(event.id), data_model)

        # Phase 1: Warming
        target_temp_p1 = target_temp - LinearRegressionService.get_temp_margin(training_adjust)
        time_p1 = 0
        # Timer(time_p1, set_event_status, (str(event.id), 'p1')).start()
        Timer(time_p1, boiler_adapter.warm, (1.0,)).start()

        data_model['p1'] = {'time': time_p1, 'target_temp': target_temp_p1, 'init_temp': temperature,
                            'final_temp_rad': max_temp_rad, 'x': side_x, 'y': side_y, 'temp_ext': out_temp}

        # Phase 2: Inertia warming
        model_p2 = LinearRegressionService.get_time_model(training_warm, test_warm)
        time_p2 = LinearRegressionService.predict_time(model=model_p2, x=side_x, y=side_y, temp_ini=temperature,
                                                       temp_fin=target_temp_p1, temp_ext=out_temp, temp_rad=max_temp_rad)[0]

        target_temp_p2 = target_temp
        Timer(time_p2, boiler_adapter.rest, ()).start()
        Timer(time_p2-1, set_event_status, (str(event.id), 'p2')).start()

        # Fase 3: Keep
        model_temp_p3 = LinearRegressionService.get_temp_rad_model(training_adjust, test_adjust)
        temp_rad_final_p3 = LinearRegressionService.predict_temp_rad(model=model_temp_p3, x=side_x, y=side_y,
                                                                     temp_ini=target_temp_p1, temp_fin=target_temp_p2, temp_ext=out_temp,
                                                                     temp_rad_inicial=max_temp_rad)[0]
        model_p3 = LinearRegressionService.get_time_model(training_adjust, test_adjust)
        time_p3 = time_p2 + LinearRegressionService.predict_pause_time(model=model_p3, x=side_x, y=side_y,
                                                                       temp_ini=target_temp_p1, temp_fin=target_temp_p2, temp_ext=out_temp,
                                                                       temp_rad_inicial=max_temp_rad, temp_rad_final=temp_rad_final_p3)[0]
        power_p3 = calculate_power(temp_rad_final_p3, min_temp_rad, max_temp_rad)

        Timer(time_p3, boiler_adapter.warm, (power_p3,)).start()
        Timer(time_p3-1, set_event_status, (str(event.id), 'p3')).start()

        # Timer(time_p3 + 10, write_event, (str(event.id),)).start()


        data_model['p2'] = {'time': time_p2, 'target_temp': target_temp_p2, 'init_temp': target_temp_p1, 'init_temp_rad': max_temp_rad,
                            'final_temp_rad': temp_rad_final_p3, 'x': side_x, 'y': side_y, 'temp_ext': out_temp}

        data_model['p3'] = {'time': time_p3, 'target_temp': target_temp_p2, 'init_temp': target_temp_p2,
                            'final_temp_rad': temp_rad_final_p3, 'x': side_x, 'y': side_y, 'temp_ext': out_temp}

        print('FASE: CALENTAR')
        print('Temperatura inicial : ' + str(temperature))
        print('Temperatura final   : ' + str(target_temp_p1))
        print('Temperatura radiador: ' + str(max_temp_rad))
        print('Inicio fase:        : ' + str(time_p1))

        print('FASE: INERCIA')
        print('Temperatura inicial : ' + str(target_temp_p1))
        print('Temperatura final   : ' + str(target_temp_p2))
        print('Temp.radiador init  : ' + str(max_temp_rad))
        print('Temp.radiador final : ' + str(temp_rad_final_p3))
        print('Inicio fase:        : ' + str(time_p2))

        print('FASE: MANTENER')
        print('Potencia radiador   : ' + str(power_p3))
        print('Inicio fase:        : ' + str(time_p3))
        print(str(data_model))

        cache.set(str(event.id), data_model)

        return data_model


def calculate_power(temp_rad, min_temp_rad, max_temp_rad):
    if (temp_rad >= min_temp_rad and temp_rad <= max_temp_rad):
        return 1.0 - ((max_temp_rad - temp_rad) / (max_temp_rad - min_temp_rad))
    else:
        return 1.0


def sim_program(boiler_adapter, temperature, temp_min, temp_max):
    if (boiler_adapter.switched_on == True):
        if (temperature <= temp_min):
            if (boiler_adapter.warming_up == True):
                pass
            elif (boiler_adapter.warming_up == False):
                boiler_adapter.warming_up = True
                Timer(3, boiler_adapter.warm, (1.0,)).start()
            else:
                boiler_adapter.warming_up = True
                Timer(3, boiler_adapter.warm, (1.0,)).start()
        elif (temperature >= temp_max):
            if (boiler_adapter.warming_up == True):
                boiler_adapter.warming_up = False
                Timer(3, boiler_adapter.rest, ()).start()
            elif (boiler_adapter.warming_up == False):
                pass
            else:
                boiler_adapter.warming_up = False
                Timer(3, boiler_adapter.rest, ()).start()
        else:
            if (boiler_adapter.warming_up == True):
                pass
            elif (boiler_adapter.warming_up == False):
                pass
            else:
                boiler_adapter.warming_up = True
                Timer(3, boiler_adapter.warm, (1.0,)).start()
    else:
        boiler_adapter.warming_up = None


def normal_program(boiler_adapter, temperature, temp_min, temp_max):
    if (boiler_adapter.switched_on == True):
        if (temperature <= temp_min):
            if (boiler_adapter.warming_up == True):
                pass
            elif (boiler_adapter.warming_up == False):
                boiler_adapter.warming_up = True
                Timer(3, boiler_adapter.warm, (1.0,)).start()
            else:
                boiler_adapter.warming_up = True
                Timer(3, boiler_adapter.warm, (1.0,)).start()
        elif (temperature >= temp_max):
            if (boiler_adapter.warming_up == True):
                boiler_adapter.warming_up = False
                Timer(3, boiler_adapter.rest, ()).start()
            elif (boiler_adapter.warming_up == False):
                pass
            else:
                boiler_adapter.warming_up = False
                Timer(3, boiler_adapter.rest, ()).start()
        else:
            if (boiler_adapter.warming_up == True):
                pass
            elif (boiler_adapter.warming_up == False):
                pass
            else:
                boiler_adapter.warming_up = True
                Timer(3, boiler_adapter.warm, (1.0,)).start()
    else:
        boiler_adapter.warming_up = None


def check_temperature(boiler_adapter):
    boiler_adapter.residence.temperature = PiService.get_temp()
    if (boiler_adapter.switched_on == True):
        Timer(5, check_temperature, (boiler_adapter,)).start()


def set_event_status(event_id, status):
    data_event = cache.get(event_id)
    if data_event is None:
        data_event = {}

    data_event['status'] = status
    print(data_event)
    cache.set(event_id, data_event)


def write_event(scheduler_event):
    if scheduler_event.scheduler_residence.residence.boiler.type == 'BoilerAdapterSmart':

        training_warm = 'simulation_on_heat_200.csv'
        test_warm = 'simulation_on_heat_10.csv'
        training_adjust = 'simulation_off_heat_200.csv'
        test_adjust = 'simulation_off_heat_10.csv'

        training_warm_path = os.path.join(MEDIA_ROOT, 'simulation/' + training_warm)
        training_adjust_path = os.path.join(MEDIA_ROOT, 'simulation/' + training_adjust)

        event_root = os.path.join(MEDIA_ROOT, 'event')
        event_dir = os.path.join(event_root, str(scheduler_event.event.id))

        event_path = os.path.join(event_dir, 'audit.csv')

        if os.path.exists(event_path):
            df = pd.read_csv(event_path)

            df_p1 = df[df.Phase == 'p1']
            df_p2 = df[df.Phase == 'p2']
            df_p3 = df[df.Phase == 'p3']

            temp1 = df_p1['Temperature']
            t1 = df_p1['Time'].map(lambda x: datetime.datetime.strptime(str(x), '%Y-%m-%dT%H:%M:%S.%fZ'))

            temp2 = df_p2['Temperature']
            t2 = df_p2['Time'].map(lambda y: datetime.datetime.strptime(str(y), '%Y-%m-%dT%H:%M:%S.%fZ'))

            if os.path.exists(training_warm_path):
                with open(training_warm_path, 'a') as csvfile:
                    p1 = cache.get(str(scheduler_event.event.id))['p1']

                    begin_phase = (t1[0] - datetime.datetime.utcfromtimestamp(0)).total_seconds()
                    end_phase = (t1[t1.size - 1] - datetime.datetime.utcfromtimestamp(0)).total_seconds()

                    print (end_phase - begin_phase)
                    filewriter = csv.writer(csvfile, delimiter=str(','), quotechar=str('|'), quoting=csv.QUOTE_MINIMAL)
                    filewriter.writerow([str(scheduler_event.event.id), p1['x'], p1['y'], temp1[0], temp1[temp1.size-1],
                                         p1['temp_ext'], p1['final_temp_rad'], end_phase - begin_phase])

            if os.path.exists(training_adjust_path):
                with open(training_adjust_path, 'a') as csvfile:
                    p2 = cache.get(str(scheduler_event.event.id))['p2']

                    begin_phase = (t2[t1.size] - datetime.datetime.utcfromtimestamp(0)).total_seconds()
                    end_phase = (t2[t1.size + t2.size - 1] - datetime.datetime.utcfromtimestamp(0)).total_seconds()

                    print(end_phase - begin_phase)
                    filewriter = csv.writer(csvfile, delimiter=str(','), quotechar=str('|'), quoting=csv.QUOTE_MINIMAL)
                    filewriter.writerow([str(scheduler_event.event.id), p2['x'], p2['y'], temp2[temp1.size],
                                         temp2[t1.size + t2.size - 1], p2['temp_ext'],
                                         p2['init_temp_rad'],  p2['final_temp_rad'], end_phase - begin_phase])





def write_event_test(id):
    training_warm = 'simulation_on_heat_200.csv'
    training_adjust = 'simulation_off_heat_200.csv'

    training_warm_path = os.path.join(MEDIA_ROOT, 'simulation/' + training_warm)
    training_adjust_path = os.path.join(MEDIA_ROOT, 'simulation/' + training_adjust)

    event_root = os.path.join(MEDIA_ROOT, 'event')
    event_dir = os.path.join(event_root, str(id))

    event_path = os.path.join(event_dir, 'audit.csv')

    if os.path.exists(event_path):
        df = pd.read_csv(event_path)

        df_p1 = df[df.Phase == 'p1']
        df_p2 = df[df.Phase == 'p2']

        temp1 = df_p1['Temperature'].values
        # t1 = df_p1['Time'].map(lambda x: datetime.datetime.strptime(str(x), '%Y-%m-%dT%H:%M:%S.%fZ'))
        # t1v = t1.values
        time1 = df_p1['Time'].map(lambda x: (datetime.datetime.strptime(str(x), '%Y-%m-%dT%H:%M:%S.%fZ') -
                                             datetime.datetime.utcfromtimestamp(0)).total_seconds()).values
        temp2 = df_p2['Temperature'].values
        # t2 = df_p2['Time'].map(lambda y: datetime.datetime.strptime(str(y), '%Y-%m-%dT%H:%M:%S.%fZ'))
        # t2v = t2.values
        time2 = df_p2['Time'].map(lambda x: (datetime.datetime.strptime(str(x), '%Y-%m-%dT%H:%M:%S.%fZ') -
                                             datetime.datetime.utcfromtimestamp(0)).total_seconds()).values
        #
        # print(temp1[0])
        # print(t1[0])
        # print(t1v[0])
        # print(time1[0])
        # print(time1[-1])
        #
        # print(temp2[0])
        # print(t2[0])
        # print(t2v[0])

        if os.path.exists(training_warm_path):
            with open(training_warm_path, 'a') as csvfile:
                # p1 = cache.get(str(id))['p1']

                # begin_phase = (t1[0] - datetime.datetime.utcfromtimestamp(0)).total_seconds()
                # end_phase = (t1[t1.size - 1] - datetime.datetime.utcfromtimestamp(0)).total_seconds()

                begin_phase = time1[0]
                end_phase = time1[-1]

                print (end_phase - begin_phase)
                filewriter = csv.writer(csvfile, delimiter=str(','), quotechar=str('|'), quoting=csv.QUOTE_MINIMAL)
                filewriter.writerow([str(id), 3, 4, temp1[0], temp1[-1], 297,
                                     311, end_phase - begin_phase])

        if os.path.exists(training_adjust_path):
            with open(training_adjust_path, 'a') as csvfile:
                # p2 = cache.get(str(id))['p2']

                # begin_phase = (t2[t1.size] - datetime.datetime.utcfromtimestamp(0)).total_seconds()
                # end_phase = (t2[t1.size + t2.size - 1] - datetime.datetime.utcfromtimestamp(0)).total_seconds()

                begin_phase = time2[0]
                end_phase = time2[-1]

                print(end_phase - begin_phase)
                filewriter = csv.writer(csvfile, delimiter=str(','), quotechar=str('|'), quoting=csv.QUOTE_MINIMAL)
                filewriter.writerow([str(id), 3, 4, temp2[0], temp2[-1], 297,
                                     321,  321, end_phase - begin_phase])



