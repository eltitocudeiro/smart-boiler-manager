

class scheduler_sigleton:

    __instance = None
    __scheduler = None

    @staticmethod
    def get_instance():
        if scheduler_sigleton.__instance == None:
            scheduler_sigleton()
        return scheduler_sigleton.__instance

    def __init__(self):
        if scheduler_sigleton.__instance != None:
            raise Exception('This class is a singleton')
        else:
            scheduler_sigleton.__instance = self
