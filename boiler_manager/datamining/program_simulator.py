# -*- coding: utf-8 -*-
from __future__ import print_function

import numpy as np
from random import randrange, uniform
import csv
import os

from smart_boiler_manager.settings import MEDIA_ROOT, STATIC_ROOT


def evolve_ts(u, ui):
    """
    This function uses a numpy expression to
    evaluate the derivatives in the Laplacian, and
    calculates u[i,j] based on ui[i,j].
    """
    u[1:-1, 1:-1] = ui[1:-1, 1:-1] + a * dt * ((ui[2:, 1:-1] - 2 * ui[1:-1, 1:-1] + ui[:-2, 1:-1]) / dx2 + (
            ui[1:-1, 2:] - 2 * ui[1:-1, 1:-1] + ui[1:-1, :-2]) / dy2)


# Constant values
dx = 0.01  # Interval size in x-direction.
dy = 0.01  # Interval size in y-direction.
a = 0.2167  # .19           # Diffusion constant.
dx2 = dx ** 2  # To save CPU cycles, we'll compute Delta x^2
dy2 = dy ** 2  # and Delta y^2 only once and store them.
# For stability, this is the largest interval possible
# for the size of the time-step:
dt = dx2 * dy2 / (2 * a * (dx2 + dy2))


def thermo_on_heat_simulation(n_sim, csv_name):
    # CSV
    with open(csv_name + '.csv', 'wb') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        filewriter.writerow(['Simulation', 'X_side', 'Y_side', 'temp_media_inicial',
                             'temp_media_final', 'temp_pared', 'temp_rad', 't'])

        n = 0
        while (n < n_sim):

            side_x = randrange(2, 5)
            side_y = randrange(2, 5)

            temp_amb = uniform(278, 288)
            temp_obj = uniform(289, 294)
            temp_ext = uniform(278, 288)
            temp_rad = uniform(343, 373)

            nx = int(side_x / dx)
            ny = int(side_y / dy)

            # Start u and ui off as zero matrices:
            ui = np.zeros([nx, ny])
            u = np.zeros([nx, ny])

            # Now, set the initial conditions (ui).
            ui[:, :] = temp_amb
            ui[0, :] = temp_rad
            ui[-1, :] = temp_ext
            ui[:, 0] = temp_ext
            ui[:, -1] = temp_ext
            u = ui.copy()

            var_s = n
            var_side_x = side_x
            var_side_y = side_y
            var_temp_media_inicial = ui[1:-1, 1:-1].mean()
            var_temp_pared = temp_ext
            var_temp_rad = temp_rad

            m = 0
            prev_sim = 0
            while True:
                evolve_ts(u, ui)
                ui = u.copy()
                m += 1
                if (m % 1000) == 999:
                    print('N: ' + str(n) + ' - ' + 'm: ' + str(m) + ' - ui.mean():' + str(ui[1:-1, 1:-1].mean()))

                d = temp_obj - ui[1:-1, 1:-1].mean()
                if d < 0.0: break
                if (ui[1:-1, 1:-1].mean() - prev_sim) < 0.001:
                    break
                else:
                    prev_sim = ui[1:-1, 1:-1].mean()

            if m < 1000:
                continue

            var_temp_media_final = ui[1:-1, 1:-1].mean()
            var_t = m
            filewriter.writerow(
                [var_s, var_side_x, var_side_y, var_temp_media_inicial, var_temp_media_final, var_temp_pared,
                 var_temp_rad, var_t])
            n += 1


def thermo_off_heat_simulation(n_sim, csv_name):
    # CSV
    with open(csv_name + '.csv', 'wb') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        filewriter.writerow(['Simulation', 'X_side', 'Y_side', 'temp_media_inicial',
                             'temp_media_final', 'temp_pared', 'temp_rad_inicial', 'temp_rad_final', 't'])

        n = 0
        while (n < n_sim):

            side_x = randrange(2, 5)
            side_y = randrange(2, 5)

            temp_amb = uniform(278, 288)
            temp_obj = uniform(289, 294)
            temp_ext = uniform(278, 288)
            temp_rad = uniform(343, 373)

            nx = int(side_x / dx)
            ny = int(side_y / dy)

            # Start u and ui off as zero matrices:
            ui = np.zeros([nx, ny])
            u = np.zeros([nx, ny])

            # Now, set the initial conditions (ui).
            ui[:, :] = temp_amb
            ui[0, :] = temp_rad
            ui[-1, :] = temp_ext
            ui[:, 0] = temp_ext
            ui[:, -1] = temp_ext
            u = ui.copy()

            var_s = n
            var_side_x = side_x
            var_side_y = side_y
            var_temp_media_inicial = ui[1:-1, 1:-1].mean()
            var_temp_pared = temp_ext
            var_temp_rad_inicial = temp_rad

            m = 0
            prev_sim = 0
            while True:
                evolve_ts(u, ui)
                ui = u.copy()
                ui_mean = ui[1:-1, 1:-1].mean()
                if temp_rad > temp_ext:
                    temp_rad *= 0.999
                    ui[0, :] = temp_rad
                else:
                    temp_rad = temp_ext

                m += 1
                if (m % 1000) == 999:
                    print('m: ' + str(m) + ' - ui.mean():' + str(ui[1:-1, 1:-1].mean()))

                if (ui[1:-1, 1:-1].mean() - prev_sim) < 0.001:
                    print('BREAK: this=' + str(ui_mean) + ' - prev=' + str(prev_sim))
                    break
                else:
                    prev_sim = ui[1:-1, 1:-1].mean()

            var_temp_media_final = ui[1:-1, 1:-1].mean()
            var_temp_rad_final = temp_rad
            var_t = m
            filewriter.writerow(
                [var_s, var_side_x, var_side_y, var_temp_media_inicial, var_temp_media_final, var_temp_pared,
                 var_temp_rad_inicial, var_temp_rad_final, var_t])
            n += 1


def thermo_off_cool_simulation(n_sim, csv_name):
    # CSV
    with open(csv_name + '.csv', 'wb') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        filewriter.writerow(['Simulation', 'X_side', 'Y_side', 'temp_media_inicial',
                             'temp_media_final', 'temp_pared', 'temp_rad', 't'])

        n = 0
        while (n < n_sim):

            side_x = randrange(2, 5)
            side_y = randrange(2, 5)

            temp_obj = uniform(278, 288)
            temp_amb = uniform(289, 294)
            temp_ext = uniform(278, 288)
            #        temp_rad = uniform(343, 373)

            nx = int(side_x / dx)
            ny = int(side_y / dy)

            # Start u and ui off as zero matrices:
            ui = np.zeros([nx, ny])
            u = np.zeros([nx, ny])

            # Now, set the initial conditions (ui).
            ui[:, :] = temp_amb
            ui[0, :] = temp_ext
            ui[-1, :] = temp_ext
            ui[:, 0] = temp_ext
            ui[:, -1] = temp_ext
            u = ui.copy()

            var_s = n
            var_side_x = side_x
            var_side_y = side_y
            var_temp_media_inicial = ui[1:-1, 1:-1].mean()
            var_temp_pared = temp_ext
            var_temp_rad = temp_ext

            m = 0
            prev_sim = 0
            while True:
                evolve_ts(u, ui)
                ui = u.copy()
                m += 1
                if (m % 1000) == 999:
                    print('m: ' + str(m) + ' - ui.mean():' + str(ui[1:-1, 1:-1].mean()))

                d = ui[1:-1, 1:-1].mean() - temp_obj
                if d < 0.0: break
                if (abs(ui[1:-1, 1:-1].mean() - prev_sim)) < 0.001:
                    break
                else:
                    prev_sim = ui[1:-1, 1:-1].mean()

            var_temp_media_final = ui[1:-1, 1:-1].mean()
            var_t = m
            filewriter.writerow(
                [var_s, var_side_x, var_side_y, var_temp_media_inicial, var_temp_media_final, var_temp_pared,
                 var_temp_rad, var_t])
            n += 1

if __name__ == '__main__':
    # path = os.path.join(STATIC_ROOT, 'simulation/' + 'simulation_on_heat_50')
    # thermo_on_heat_simulation(50, path)
    # path = os.path.join(STATIC_ROOT, 'simulation/' + 'simulation_on_heat_200')
    # thermo_on_heat_simulation(200, path)
    # path = os.path.join(STATIC_ROOT, 'simulation/' + 'simulation_off_heat_200')
    # thermo_off_heat_simulation(200, path)
    # path = os.path.join(STATIC_ROOT, 'simulation/' + 'simulation_off_heat_50')
    # thermo_off_heat_simulation(50, path)
    # path = os.path.join(STATIC_ROOT, 'simulation/' + 'simulation_off_cool_200')
    # thermo_off_cool_simulation(200, path)
    path = os.path.join(STATIC_ROOT, 'simulation/' + 'simulation_off_cool_50')
    thermo_off_cool_simulation(50, path)