from __future__ import print_function
import httplib2
import os
from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from datetime import datetime
import datetime, time

from smart_boiler_manager.settings import STATIC_ROOT

try:
    import argparse

    #    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
    flags = tools.argparser.parse_args([])
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/calendar-python-quickstart.json
SCOPE_READ_ONLY = 'https://www.googleapis.com/auth/calendar.readonly'
SCOPE = 'https://www.googleapis.com/auth/calendar'
GOOGLE_ROOT = os.path.join(STATIC_ROOT, 'google')
CLIENT_SECRET_FILE = os.path.join(GOOGLE_ROOT, 'client_secret.json')
APPLICATION_NAME = 'Google Calendar API Python'


def get_credentials(readOnly=True):
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    # home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(GOOGLE_ROOT, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    if readOnly:
        credential_path = os.path.join(credential_dir,
                                       'google-calendar-readonly-python.json')
    else:
        credential_path = os.path.join(credential_dir,
                                       'google-calendar-python.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        if readOnly:
            flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPE_READ_ONLY)
        else:
            flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPE)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else:  # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials


def getService(readOnly=True):
    credentials = get_credentials(readOnly)
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('calendar', 'v3', http=http)

    return service


def getCalendar(calendarId):
    service = getService(True)
    return service.calendars().get(calendarId=calendarId).execute()


def createCalendar(name):
    service = getService(False)

    calendar = {
        'summary': name,
        'timeZone': 'Europe/Madrid'
    }

    return service.calendars().insert(body=calendar).execute()


def getEvents(calendarId, nextEvents):
    service = getService()

    now = datetime.datetime.utcnow().isoformat() + 'Z'  # 'Z' indicates UTC time
    print('Getting the upcoming ' + str(nextEvents) + ' events.')
    eventsResult = service.events().list(
        calendarId=calendarId, timeMin=now, maxResults=nextEvents, singleEvents=True,
        orderBy='startTime').execute()

    return eventsResult.get('items', [])


def getEvent(calendarId, eventId):
    service = getService()
    event = service.events().get(calendarId=calendarId, eventId=eventId).execute()

    return event


def setEvent(calendarId, summary, minute_begin, minute_end, hour_begin,
              hour_end, day_begin, day_end, month_begin, month_end, year_begin, year_end,
              location, recurrence, description):
    service = getService(False)

    begin = datetime.datetime.utcnow()
    begin = begin.replace(year=year_begin, month=month_begin, day=day_begin, hour=hour_begin,
                          minute=minute_begin, second=0, microsecond=0)
    end = datetime.datetime.utcnow()
    end = end.replace(year=year_end, month=month_end, day=day_end, hour=hour_end,
                      minute=minute_end, second=0, microsecond=0)

    event = {
        'summary': summary,
        'location': location,
        'description': description,
        'start': {
            'dateTime': begin.isoformat(),
            'timeZone': 'Europe/Madrid',
        },
        'end': {
            'dateTime': end.isoformat(),
            'timeZone': 'Europe/Madrid',
        },
        'recurrence': [
            'RRULE:FREQ=DAILY;COUNT=' + str(recurrence)
        ],
        'reminders': {
            'useDefault': False,
            'overrides': [
                {'method': 'email', 'minutes': 10},
                {'method': 'popup', 'minutes': 10},
            ],
        },
    }

    event = service.events().insert(calendarId=calendarId, body=event).execute()
    print('Events created: %s' % (event.get('htmlLink')))

    return event


def setAcl(calendarId, email, role):
    service = getService(False)
    rule = {
        "role": role,
        "scope": {
            "type": "user",
            "value": email
        }
    }

    return service.acl().insert(calendarId=calendarId, body=rule).execute()


def deleteCalendar(calendarId):
    service = getService(False)
    service.calendars().delete(calendarId=calendarId).execute()


def clearCalendar(calendarId):
    service = getService(False)
    service.calendars().clear(calendarId=calendarId).execute()


def delayTime(stringTime):
    # (abs(time.altzone) // 3600)-1 for StandardTime
    # FIX!!!!
    start_time = (datetime.datetime.strptime(stringTime,
            '%Y-%m-%dT%H:%M:%S{}{:0>2}:{:0>2}'.format('-' if time.altzone > 0 else '+',
            (abs(time.altzone) // 3600), abs(time.altzone // 60) % 60)))
    dt = datetime.datetime.now()
    dt_now = ((dt - datetime.datetime.utcfromtimestamp(0)).total_seconds())
    return (start_time - datetime.datetime.utcfromtimestamp(0)).total_seconds(), \
           ((start_time - datetime.datetime.utcfromtimestamp(0)).total_seconds() - dt_now)


def main():
    """Shows basic usage of the Google Calendar API.

    Creates a Google Calendar API service object and outputs a list of the next
    10 events on the user's calendar.
    """


    # email = 'a.agra.lorenzo@gmail.com'
    # id = 'hnodqbn4hjs4929d70cqtgsg94@group.calendar.google.com'
    # events = getEvents(id, 10)
    # if not events:
    #     print('No upcoming events found.')
    # for event in events:
    #     start = event['start'].get('dateTime', event['start'].get('date'))
    #     end = event['end'].get('dateTime', event['end'].get('date'))
    #     print('Event[' + str(event['id']) + '] - Start: ' + str(event['start']) + 'End: ' + str(event['end']))
    #
    created_calendar = createCalendar('Termo-Manager')
    # id = created_calendar['id']
    #  email = 'a.agra.lorenzo@gmail.com'
    #  defaultEvent = setDefaultEvents(id, 'Termo-Manager Default Month', email)
    #   event = setEvents(id, 'Termo-Manager Month', 0, 0, 12,
    #       14, 10, 10, 5, 5, 2017, 2017,
    #       'A Coruna, Galicia, Spain', 10, 'A termostat calendar.')
    #    created_rule = setAcl(id, email, "writer")

    #    service = getService()
    #   service2 = getService(False)

    #    events = getEvents('primary', 10)
    #  if not events:
    #      print('No upcoming events found.')
    #  for event in events:
    #       start = event['start'].get('dateTime', event['start'].get('date'))
    #        print(start, event['summary'])

    #   calendar_list_entry = service.calendarList().get(calendarId='primary').execute()
    # primaryId = calendar_list_entry['id']
    # print (primaryId)

    # calendar_list_entry = service.calendarList().get(calendarId=primaryId).execute()
    #  print(calendar_list_entry['summary'])

    #    created_calendar = createCalendar('Termo-Manager')
    #   print(created_calendar['id'])
    #  id = created_calendar['id']

    #  calendar_list_entry = service.calendarList().get(calendarId=id).execute()
    #   print(calendar_list_entry['summary'])

    # calendar_list_entry = {
    #     'id': id,
    #     "defaultReminders": [
    #         {
    #             "method": "email",
    #             "minutes": 0
    #         }
    #     ],
    #     "notificationSettings": {
    #         "notifications": [
    #             {
    #                 "method": "email",
    #                 "type": "eventCreation"
    #             }
    #         ]
    #     }
    # }


#   created_calendar_list_entry = service2.calendarList().insert(body=calendar_list_entry).execute()
#   print (created_calendar_list_entry['summary'])

#  calendar_list_entry = service.calendarList().get(calendarId=id).execute()
#  print(calendar_list_entry['summary'])


#    events = getEvents(id, 10)
#    if not events:
#       print('No upcoming events found.')
#  for event in events:
#     start = event['start'].get('dateTime', event['start'].get('date'))
#    print(start, event['summary'])

#  event = setDefaultEvents(id, 'Termo-Manager Default Month')


# events = getEvents(id, 10)
# if not events:
#      print('No upcoming events found.')
#   for event in events:
#        start = event['start'].get('dateTime', event['start'].get('date'))
#        print(start, event['summary'])


# created_rule = setAcl(id, "a.agra.lorenzo@gmail.com", "writer")
# print(created_rule['id'])


# clearCalendar(primaryId)
#  events = getEvents('primary', 10)
#  if not events:
#      print('No upcoming events found.')
#   for event in events:
#        start = event['start'].get('dateTime', event['start'].get('date'))
#        print(start, event['summary'])

# clearCalendar(id)
# events = getEvents(id, 10)
# if not events:
#     print('No upcoming events found.')
#  for event in events:
#       start = event['start'].get('dateTime', event['start'].get('date'))
#        print(start, event['summary'])

# deleteCalendar(id)
# calendar_list_entry = service.calendarList().get(calendarId=id).execute()
#  print(calendar_list_entry['summary'])


if __name__ == '__main__':
    main()
