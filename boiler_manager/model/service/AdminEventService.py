# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from boiler_manager.scheduler import scheduler


def init_event_scheduler(event_list):
    return scheduler.bootScheduler(event_list=event_list)