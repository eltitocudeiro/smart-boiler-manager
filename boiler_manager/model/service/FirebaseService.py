import pyrebase
import base64

from requests import HTTPError

from smart_boiler_manager.settings import FIREBASE_CONFIG, FIREBASE_EMAIL, FIREBASE_ENCRYPT_PASSWD

config = FIREBASE_CONFIG
email = FIREBASE_EMAIL
passwd = FIREBASE_ENCRYPT_PASSWD

def get_authentication_and_db(config, email, password):
    firebase = pyrebase.initialize_app(config)
    auth = firebase.auth()
    user = auth.sign_in_with_email_and_password(email, password)
    db = firebase.database()

    return db, user

def connect_firebase(config):
    fb = pyrebase.initialize_app(config)
    return fb

def get_authenticated_user(firebase, email, password):
    auth = firebase.auth()
    user = auth.sign_in_with_email_and_password(email, base64.b64decode(password))
    return user

def db_connection(firebase):
    db = firebase.database()
    return db

def get(key, collection, user,db):
    value = db.child(collection).get(user['idToken']).val().get(key)

def get_user_position(username):
    try:
        fb = pyrebase.initialize_app(config)

        auth = fb.auth()
        user = auth.sign_in_with_email_and_password(email, base64.b64decode(passwd))

        db = fb.database()
    except HTTPError as e:
        raise e
    position = db.child('mapPoints').get(user['idToken']).val().get(username)

    try:
        return position.get("latitude"), position.get("longitude")
    except AttributeError as e:
       raise e





if __name__ == "__main__":

    db, user = get_authentication_and_db(config, email, base64.b64decode(passwd))
    print(user)

    lat, long = get_user_position("ElTitoCudeiro", user, db)
    print 'Latitud: ' + str(lat)
    print 'Longitud: ' + str(long)

    fb = connect_firebase(config)
    user = get_authenticated_user(fb, email, passwd)
    db = db_connection(fb)

    lat, long = get_user_position("ElTitoCudeiro", user, db)
    print 'Latitud: ' + str(lat)
    print 'Longitud: ' + str(long)


