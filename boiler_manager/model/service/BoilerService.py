# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from boiler_manager.model.repository.entity import Boiler
from boiler_manager.model.repository.dao import BoilerDao


def create(brand, model, power, max_temp, min_temp, type):
    boiler = Boiler(brand=brand, model=model, power=power, max_temp=max_temp, min_temp=min_temp, type=type)
    return BoilerDao.update_or_create(boiler)

def create(boiler):
    return BoilerDao.update_or_create(boiler)


def get_all():
    return BoilerDao.get_all()


def get(id):
    return BoilerDao.get(id)
