# -*- coding: utf-8 -*-
from __future__ import print_function

import pandas as pd
from sklearn import linear_model
import numpy as np
import os

from smart_boiler_manager.settings import MEDIA_ROOT, STATIC_ROOT


def get_temp_margin(training_partition):
    training_path = os.path.join(MEDIA_ROOT, 'simulation/' + training_partition)

    print(training_path)

    data = pd.read_csv(training_path)

    final_temp_mean = data['temp_media_final'].mean()
    initial_temp_mean = data['temp_media_inicial'].mean()

    return final_temp_mean - initial_temp_mean


def get_time_model(training_partition, test_partition):
    training_path = os.path.join(MEDIA_ROOT, 'simulation/' + training_partition)
    test_path = os.path.join(MEDIA_ROOT, 'simulation/' + test_partition)

    print(training_path)

    data = pd.read_csv(training_path)
    dataTest = pd.read_csv(test_path)

    x = data.drop(['Simulation', 't'], axis=1)
    x_test = dataTest.drop(['Simulation', 't'], axis=1)

    y = data['t']
    y_test = dataTest['t']

    ols = linear_model.LinearRegression()
    model = ols.fit(x, y)

    return model


def get_temp_rad_model(training_partition, test_partition):
    training_path = os.path.join(MEDIA_ROOT, 'simulation/' + training_partition)
    test_path = os.path.join(MEDIA_ROOT, 'simulation/' + test_partition)

    print(training_path)

    data = pd.read_csv(training_path)
    dataTest = pd.read_csv(test_path)

    x = data.drop(['Simulation', 'temp_rad_final', 't'], axis=1)
    y = data['temp_rad_final']

    x_test = dataTest.drop(['Simulation', 'temp_rad_final', 't'], axis=1)
    y_test = dataTest['temp_rad_final']

    ols = linear_model.LinearRegression()
    model = ols.fit(x, y)

    return model


def get_model(training_partition, target_param):
    training_path = os.path.join(MEDIA_ROOT, training_partition)

    data = pd.read_csv(training_path)
    x = data.drop(['Simulation', target_param], axis=1)
    y = data[target_param]

    ols = linear_model.LinearRegression()
    model = ols.fit(x, y)

    return model


def predict_time(model, x, y, temp_ini, temp_fin, temp_ext, temp_rad):
    return model.predict(np.array([(x, y, temp_ini, temp_fin, temp_ext, temp_rad)]))


def predict_temp_rad(model, x, y, temp_ini, temp_fin, temp_ext, temp_rad_inicial):
    return model.predict(np.array([(x, y, temp_ini, temp_fin, temp_ext, temp_rad_inicial)]))


def predict_pause_time(model, x, y, temp_ini, temp_fin, temp_ext, temp_rad_inicial, temp_rad_final):
    return model.predict(np.array([(x, y, temp_ini, temp_fin, temp_ext, temp_rad_inicial, temp_rad_final)]))


def pearson_correlation(var1, var2, partition):
    path = os.path.join(MEDIA_ROOT, 'simulation/' + partition)

    df = pd.read_csv(path)
    r = df.corr(method="pearson")[var1][var2]

    return r

def rmse(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())



# if __name__ == '__main__':
    # print('Y_side:' + str(pearson_correlation('t', 'Y_side', partition)))
    # print('temp_media_inicial:' + str(pearson_correlation('t', 'temp_media_inicial', partition)))
    # print('temp_media_final:' + str(pearson_correlation('t', 'temp_media_final', partition)))
    # print('temp_pared:' + str(pearson_correlation('t', 'temp_pared', partition)))
    # print('temp_rad:' + str(pearson_correlation('t', 'temp_rad', partition)))
    # print('humidity:' + str(pearson_correlation('t', 'humidity', partition)))
#######################################################################################################################
    # training_path = os.path.join(MEDIA_ROOT, 'simulation/' + 'simulation_off_heat_200.csv')
    # test_path = os.path.join(MEDIA_ROOT, 'simulation/' + 'simulation_off_heat_10.csv')
    # training_path = os.path.join(STATIC_ROOT, 'simulation/' + 'simulation_on_heat_200.csv')
    # test_path = os.path.join(STATIC_ROOT, 'simulation/' + 'simulation_on_heat_50.csv')
    # training_path = os.path.join(MEDIA_ROOT, 'simulation/' + 'simulation_on_heat_200.csv')
    # test_path = os.path.join(MEDIA_ROOT, 'simulation/' + 'simulation_on_heat_10.csv')
    # training_path = os.path.join(STATIC_ROOT, 'simulation/' + 'simulation_off_heat_200.csv')
    # test_path = os.path.join(STATIC_ROOT, 'simulation/' + 'simulation_off_heat_50.csv')
    # training_path = os.path.join(STATIC_ROOT, 'simulation/' + 'simulation_off_cool_200.csv')
    # test_path = os.path.join(STATIC_ROOT, 'simulation/' + 'simulation_off_cool_50.csv')
    #
    # print(training_path)
    #
    # data = pd.read_csv(training_path)
    # dataTest = pd.read_csv(test_path)
    #
    # # print df.head()
    #
    # x = data.drop(['Simulation', 't'], axis=1)
    # y = data['t']
    #
    # x_test = dataTest.drop(['Simulation', 't'], axis=1)
    # y_test = dataTest['t']
    #
    # ols = linear_model.LinearRegression()
    # model = ols.fit(x, y)
    #
    # print(data.head()['t'])
    # print (model.predict(x)[0:5])
    #
    # print(dataTest.head()['t'])
    # print (model.predict(x_test)[0:5])
    #######################################################################################################################
    #
    # d = [0.000, 0.166, 0.333]
    # p = [0.000, 0.254, 0.998]
    #
    # print("d is: " + str(["%.8f" % elem for elem in d]))
    # print("p is: " + str(["%.8f" % elem for elem in p]))
    #
    #
    #
    #
    #
    # rmse_val = rmse(np.array(d), np.array(p))
    # print("rms error is: " + str(rmse_val))
    #######################################################################################################################
    #
    # targets = y_test[0:50]
    # predictions = model.predict(x_test)[0:50]
    #
    # print("predictions: " + str(["%.8f" % elem for elem in predictions]))
    # print("targets:     " + str(["%.8f" % elem for elem in targets]))
    #
    # rmse_val = rmse(predictions, targets)
    # print("rms error is: " + str(rmse_val))