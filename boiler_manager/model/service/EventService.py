# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime
import os
import pandas as pd
import matplotlib.pyplot as plt
from boiler_manager.model.repository.dao import EventDao
from boiler_manager.model.repository.entity import Event
from smart_boiler_manager.settings import MEDIA_ROOT, MEDIA_URL


def create(google_calendar_event, target_temp, residence, user):
    event = Event(google_calendar_event=google_calendar_event, target_temp=target_temp, residence=residence, user=user)
    return EventDao.update_or_create(event)


def update(event):
    return EventDao.update_or_create(event)


def get_all():
    return EventDao.get_all()


def get_all_by_user(user):
    return EventDao.get_all_by_user(user=user)


def get(id):
    return EventDao.get(id)


def get_all_by_residence(residence):
    return EventDao.get_all_by_residence(residence=residence)


def get_by_google_calendar_event(google_calendar_event):
    return EventDao.get_by_google_calendar_event(google_calendar_event=google_calendar_event)


def set_figure_path(event_id, figure_path):
    event = EventDao.get(event_id)
    event.figure_path = figure_path
    return EventDao.update_or_create(event)


def generate_report_image(event_id):
    event_root = os.path.join(MEDIA_ROOT, 'event')
    event_dir = os.path.join(event_root, str(event_id))
    event_path = os.path.join(event_dir, 'audit.csv')

    if os.path.exists(event_path):
        df = pd.read_csv(event_path)
        x = df['Time'].map(lambda x: datetime.strptime(str(x), '%Y-%m-%dT%H:%M:%S.%fZ'))
        y = df['Temperature']

        plt.plot(x, y)
        plt.xlabel('Time')
        plt.ylabel('Temperature')
        plt.gcf().autofmt_xdate()
        # plt.show()
        figure_path = os.path.join(event_dir, 'report_plot.png')
        plt.savefig(figure_path)
        plt.clf()

        output_path = os.path.join(MEDIA_URL, 'event/' + str(event_id) + '/report_plot.png')
        return output_path
    else:
        return None