# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from boiler_manager.model.repository.entity import Residence
from boiler_manager.model.repository.dao import ResidenceDao


def create(title, address, google_calendar, boiler, user, x_side, y_side):
    r = Residence(title=title, address=address, google_calendar=google_calendar, boiler=boiler, x_side=x_side, y_side=y_side)
    residence = ResidenceDao.update_or_create(r)
    return ResidenceDao.set_user(residence=residence, user=user)


def update(residence):
    return ResidenceDao.update_or_create(residence)


def get_all():
    return ResidenceDao.get_all()


def get_all_by_user(user):
    return ResidenceDao.get_all_by_user(user=user)


def get(id):
    return ResidenceDao.get(id)


def get_by_google_calendar(google_calendar):
    return ResidenceDao.get_by_google_calendar(google_calendar=google_calendar)


def get_valid_user_residence_list(user):
    residence_list = []
    residence_user_list = ResidenceDao.get_all_by_user(user=user)
    for residence in residence_user_list:
        if residence.google_calendar != None and residence.google_calendar != '':
            residence_list.append(residence)
    return residence_list

def get_residence_list_with_address(user):
    residence_list = []
    residence_user_list = ResidenceDao.get_all_by_user(user=user)
    for residence in residence_user_list:
        if residence.address != None and residence.address != '':
            residence_list.append(residence)
    return residence_list


def set_address(residence, address):
    residence.address = address
    return ResidenceDao.update_or_create(residence)


def set_google_calendar(residence, google_calendar):
    residence.google_calendar = google_calendar
    return ResidenceDao.update_or_create(residence)
