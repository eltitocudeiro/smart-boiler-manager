import requests

from smart_boiler_manager.settings import GOOGLE_API_KEY


def InverseGeocoding(latitud, longitud):
    url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + str(latitud) + ',' \
          + str(longitud) + '&key=' + GOOGLE_API_KEY
    print url

    try:
        p = requests.post(url)
    except:
        raise
    pjson = p.json()

    return pjson['results'][0]['formatted_address']


def Geocoding(direccion):
    url = 'https://maps.googleapis.com/maps/api/geocode/json?address='
    direccion_utf = direccion.encode('utf_8')
    print direccion_utf
    direcciones = direccion_utf.split()
    for token in direcciones:
        url = url + token + '+'
    url = url + '&key=' + GOOGLE_API_KEY
    print url

    try:
        p = requests.post(url)
    except:
        raise
    pjson = p.json()

    coords = (pjson['results'][0]['geometry']['location']['lat'], pjson['results'][0]['geometry']['location']['lng'])
    return coords


def DistanceMatrix(origin, dest, mode='driving'):
    url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' + ManageAddress(origin) + \
          '&destinations=' + ManageAddress(dest)
    if (mode == 'train'):
        url = url + '&mode=transit&transit_mode=train'
    elif (mode == 'walking'):
        url = url + '&mode=walking'
    elif (mode == 'bicycling '):
        url = url + '&mode=bicycling'
    url = url + '&key=' + GOOGLE_API_KEY

    try:
        p = requests.post(url)
    except:
        raise
    pjson = p.json()

    result = (pjson['rows'][0]['elements'][0]['distance']['text'], pjson['rows'][0]['elements'][0]['duration']['text'])
    return result

def TravelDurationInSeconds(origin, dest, mode='driving'):
    url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' + ManageAddress(origin) + \
          '&destinations=' + ManageAddress(dest)
    if (mode == 'train'):
        url = url + '&mode=transit&transit_mode=train'
    elif (mode == 'walking'):
        url = url + '&mode=walking'
    elif (mode == 'bicycling '):
        url = url + '&mode=bicycling'
    url = url + '&key=' + GOOGLE_API_KEY

    try:
        p = requests.post(url)
    except:
        raise
    pjson = p.json()

    result = pjson['rows'][0]['elements'][0]['duration']['value']
    return result


def ManageAddress(address):
    result = ''
    address_utf = address.encode('utf_8')
    address_split = address_utf.split()
    for token in address_split:
        result = result + token + '+'
    return result
