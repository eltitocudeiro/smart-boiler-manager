# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import requests

HOST = '192.168.1.139'
PORT = '8000'


def get_temp():
    url = 'http://' + HOST + ':' + PORT + '/termo/temp'
    try:
        r = requests.get(url)
    except:
        print 'Request Temp failed'
    return r.json()['temperature'] + 273.16


def set_temp(casa):
    url = 'http://' + HOST + ':' + PORT + '/termo/temp'
    try:
        r = requests.get(url)
    except:
        print 'Request Temp failed'
    casa.temperatura = r.json()['temperature'] + 273.16


def turn_on_red():
    url = 'http://' + HOST + ':' + PORT + '/termo/4'
    try:
        r = requests.get(url)
    except:
        print 'Request Red LED failed'


def turn_on_green():
    url = 'http://' + HOST + ':' + PORT + '/termo/1'
    try:
        r = requests.get(url)
    except:
        print 'Request Green LED failed'


def turn_on_blue():
    url = 'http://' + HOST + ':' + PORT + '/termo/2'
    try:
        r = requests.get(url)
    except:
        print 'Request Blue LED failed'


def turn_off_RGB():
    url = 'http://' + HOST + ':' + PORT + '/termo/0'
    try:
        r = requests.get(url)
    except:
        print 'Request Turn Off RGB failed'
