import requests
from pprint import pprint

from smart_boiler_manager.settings import WEATHER_API_KEY


def getTemperature(latitud, longitud):
    url = 'http://api.openweathermap.org/data/2.5/weather?lat=' + str(latitud) + '&lon=' + \
          str(longitud) + '&appid=' + WEATHER_API_KEY
    print url

    try:
        p = requests.post(url)
    except:
        raise
    pjson = p.json()

    # pprint(pjson['main'])

    return pjson['main']['temp']
