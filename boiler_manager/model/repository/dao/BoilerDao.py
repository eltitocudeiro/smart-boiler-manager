# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from boiler_manager.model.repository.entity import Boiler


def update_or_create(boiler):
    boiler.save()
    return boiler


def get_all():
    boilers = Boiler.objects.all()
    return boilers

def get(id):
    boiler = Boiler.objects.get(id=id)
    return boiler

