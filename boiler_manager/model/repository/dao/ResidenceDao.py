# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from boiler_manager.model.repository.entity import Residence


def update_or_create(residence):
    residence.save()
    return residence


def set_user(residence, user):
    residence.user.add(user)
    residence.save()
    return residence


def get_all():
    return Residence.objects.all()


def get(id):
    return Residence.objects.get(id=id)


def get_all_by_user(user):
    return Residence.objects.filter(user=user)


def get_by_google_calendar(google_calendar):
    return Residence.objects.filter(google_calendar=google_calendar)
