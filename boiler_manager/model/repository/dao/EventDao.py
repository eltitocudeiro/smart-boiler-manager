# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from boiler_manager.model.repository.entity import Event


def update_or_create(event):
    event.save()
    return event


def get_all():
    return Event.objects.all()


def get(id):
    return Event.objects.get(id=id)


def get_all_by_user(user):
    return Event.objects.filter(user=user)


def get_all_by_residence(residence):
    return Event.objects.filter(residence=residence)


def get_by_google_calendar_event(google_calendar_event):
    return Event.objects.get(google_calendar_event=google_calendar_event)
