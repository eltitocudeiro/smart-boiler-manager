# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User


def get_all():
    users = User.objects.all()
    return users


def get(id):
    user = User.objects.get(id=id)
    return user

