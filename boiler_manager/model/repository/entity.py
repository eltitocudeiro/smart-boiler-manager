# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


# Create your models here.


class Boiler(models.Model):
    brand = models.CharField(max_length=100, null=False)
    model = models.CharField(max_length=100, null=False)
    power = models.FloatField(null=False)
    max_temp = models.FloatField(null=True)
    min_temp = models.FloatField(null=True)
    type = models.CharField(max_length=200, null=False)

    @property
    def get_id(self):
        return self.id

    def __unicode__(self):
        return "Boiler[" + str(self.id) + "] - Brand:" + self.brand + " - Model:" + self.model


class Residence(models.Model):
    title = models.CharField(max_length=200, blank=False)
    address = models.CharField(max_length=200, null=True)
    google_calendar = models.CharField(max_length=200, null=True)
    boiler = models.ForeignKey(Boiler, null=False)
    user = models.ManyToManyField(User)
    x_side = models.FloatField(null=True)
    y_side = models.FloatField(null=True)

    @property
    def get_id(self):
        return self.id

    def __unicode__(self):
        return "Residence[" + str(self.id) + "] - Title:" + self.title


class Event(models.Model):
    google_calendar_event = models.CharField(max_length=200, null=False)
    target_temp = models.FloatField(null=False)
    residence = models.ForeignKey(Residence, null=False)
    user = models.ForeignKey(User, null=False)
    figure_path = models.CharField(max_length=200, null=True)

    @property
    def get_id(self):
        return self.id

    def __unicode__(self):
        return "Event[" + str(self.id) + "] - Residence:" + str(self.residence.id) + ' - User:' + str(self.user.id)
