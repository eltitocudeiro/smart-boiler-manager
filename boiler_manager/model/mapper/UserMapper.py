# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from boiler_manager.model.repository.entity import User


def to_user(userDTO):
    return User(id=userDTO.id, username=userDTO.username, email=userDTO.email, position=None)

