# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# from boiler_manager.model.models import Boiler as Boiler_m, Residence as Residence_m, User as User_m, Event as Event_m
from boiler_manager.service.entity import Boiler as Boiler_s, Residence as Residence_s, User as User_s, Event as Event_s


def to_boiler_from_model(boiler_m):
    return Boiler_s(id=boiler_m.id, brand=boiler_m.brand, model=boiler_m.model, power=boiler_m.power,
                    max_temp=boiler_m.max_temp, min_temp=boiler_m.min_temp, type=boiler_m.type)


def to_boiler_list_from_model(boiler_list_m):
    boiler_list_s = []
    for boiler_m in boiler_list_m:
        boiler_list_s.append(to_boiler_from_model(boiler_m))
    return boiler_list_s


def to_user_from_model(user_m):
    return User_s(id=user_m.id, username=user_m.username, email=user_m.email, position=None)


def to_residence_from_model(residence_m):
    return Residence_s(id=residence_m.id, title=residence_m.title, address=residence_m.address,
                       google_calendar=residence_m.google_calendar, boiler=residence_m.boiler,
                       users=residence_m.user.all())


def to_residence_list_from_model(residence_list_m):
    residence_list_s = []
    for residence_m in residence_list_m:
        residence_list_s.append(to_residence_from_model(residence_m))
    return residence_list_s


def to_event_from_model(event_m):
    return Event_s(id=event_m.id, google_calendar_event=event_m.google_calendar_event, target_temp=event_m.target_temp,
                   residence=to_residence_from_model(event_m.residence), user=to_user_from_model(event_m.user),
                   figure_path=event_m.figure_path)


def to_event_list_from_model(event_list_m):
    event_list_s = []
    for event_m in event_list_m:
        event_list_s.append(to_event_from_model(event_m))
    return event_list_s
