# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from boiler_manager.model.repository.entity import Boiler, Residence, User, Event


def get_field(request, field):
    return request.POST[field]


def to_boiler(request):
    brand = request.POST['brand']
    model = request.POST['model']
    power = float(request.POST['power'])
    max_temp = float(request.POST['max_temp'])
    min_temp = float(request.POST['min_temp'])
    type = request.POST['type']
    b = Boiler(brand=brand, model=model, power=power, max_temp=max_temp, min_temp=min_temp, type=type)
    return b


def to_event(request):
    fechaInicial = request.POST['fecha-inicial']
    horaInicial = request.POST['hora-inicial']
    horaFinal = request.POST['hora-final']
    rec = request.POST['rec']
    target_temp = request.POST['target-temp']

    yyy1 = int(fechaInicial[0:4])
    m1 = int(fechaInicial[5:7])
    d1 = int(fechaInicial[8:])

    h1 = int(horaInicial[0:2])
    M1 = int(horaInicial[3:5])
    h2 = int(horaFinal[0:2])
    M2 = int(horaFinal[3:5])

    d2 = d1
    m2 = m1
    yyy2 = yyy1
    if h1 > h2:
        d2 += 1
        if m2 in [4, 6, 9, 11]:
            if d2 > 30:
                d2 = 1
                m2 += 1
        elif m2 == 2:
            if ((yyy2 % 4) != 0):
                if d2 > 28:
                    d2 = 1
                    m2 += 1
            else:
                if d2 > 29:
                    d2 = 1
                    m2 += 1
        elif m2 == 12:
            if d2 > 31:
                d2 = 1
                m2 = 1
                yyy2 += 1
        else:
            if d2 > 31:
                d2 = 1
                m2 += 1

    begin = {}
    begin['year'] = yyy1
    begin['month'] = m1
    begin['day'] = d1
    begin['hour'] = h1
    begin['minute'] = M1

    end = {}
    end['year'] = yyy2
    end['month'] = m2
    end['day'] = d2
    end['hour'] = h2
    end['minute'] = M2

    result = {}
    result['rec'] = int(rec)
    result['temperature'] = int(target_temp)
    result['begin'] = begin
    result['end'] = end

    return result

