# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from boiler_manager.model.service import AdminEventService
from django.core.cache import cache


def scheduler(request):
    context = {}
    cache_list = cache.get('cache_list')
    print(cache_list)
    context['cache_list'] = cache_list

    # if request.POST.has_key('begin'):
    #     event_list = AdminEventService.init_event_scheduler(event_list=None)
    #     context = {'event_list': event_list}

    return render(request, 'boiler_manager/scheduler.html', context)

