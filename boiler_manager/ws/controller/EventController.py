# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

import datetime

from django.db import OperationalError
from django.shortcuts import render
from googleapiclient.errors import HttpError

from boiler_manager.model.repository.entity import Residence
from boiler_manager.model.service import EventService, ResidenceService, GoogleCalendarService
from boiler_manager.scheduler import scheduler
from boiler_manager.ws.mapper import RequestMapper
from smart_boiler_manager.settings import MEDIA_ROOT


def event(request):
    context = {}
    if request.POST:
        if request.POST['residence'] != '' and request.POST['fecha-inicial'] != '' \
                and request.POST['hora-inicial'] != '' and request.POST['hora-final'] != '' \
                and request.POST['rec'] != '' and request.POST['target-temp'] != '':
            try:
                residence_id = int(RequestMapper.get_field(request, 'residence'))
                residence = ResidenceService.get(id=residence_id)
                if residence.address:
                    data_event = RequestMapper.to_event(request=request)
                    calendar = GoogleCalendarService.getCalendar(calendarId=residence.google_calendar)
                    now = datetime.datetime.utcnow().isoformat() + 'Z'
                    summary = 'Boiler Manager Event in Residence:' + str(residence.id) + '\n'
                    description = 'Residence Id = ' + str(residence.id) + '\n' + 'User = ' + str(request.user) + '\n' \
                                  + 'Datetime = ' + str(now) + '\n' + 'Target temperature = ' \
                                  + str(data_event['temperature']) + '\n'
                    created_event = GoogleCalendarService.setEvent(calendarId=calendar['id'], summary=summary,
                            minute_begin=data_event['begin']['minute'], minute_end=data_event['end']['minute'],
                            hour_begin=data_event['begin']['hour'], hour_end=data_event['end']['hour'],
                            day_begin=data_event['begin']['day'], day_end=data_event['end']['day'],
                            month_begin=data_event['begin']['month'], month_end=data_event['end']['month'],
                            year_begin=data_event['begin']['year'], year_end=data_event['end']['year'],
                            location=residence.address, recurrence=data_event['rec'], description=description)
                    event = EventService.create(google_calendar_event=created_event['id'],
                            target_temp=data_event['temperature'], residence=residence, user=request.user)
                    context = {'e': event}
                    error_message = 'Evento: ' + str(event.id) + ' creado correctamente.'
                    context['error_message'] = error_message
                    return render(request, 'boiler_manager/event/event_detail.html', context)
                else:
                    error_message = 'Residencia no geolocalizada.'
                    context['error_message'] = error_message
            except Residence.DoesNotExist:
                error_message = 'Residencia no encontrada en el sistema.'
                context['error_message'] = error_message
            except ValueError:
                error_message = 'Datos no válidos.'
                context['error_message'] = error_message
            except HttpError:
                error_message = 'Calendario no válido. Establezca uno de nuevo.'
                context['error_message'] = error_message
        else:
            error_message = 'Datos no válidos.'
            context['error_message'] = error_message
    residence_list = ResidenceService.get_valid_user_residence_list(request.user)
    context['residence_list'] = residence_list
    return render(request, 'boiler_manager/event/event.html', context)


def events(request):
    try:
        if request.user.is_staff:
            event_list = EventService.get_all()
            context = {'event_list': event_list}
        elif request.user.is_authenticated:
            event_list = EventService.get_all_by_user(user=request.user)
            context = {'event_list': event_list}
        else:
            context = {}
    except OperationalError:
        context = {}
    return render(request, 'boiler_manager/event/events.html', context)


def event_detail(request, event_id):
    e = EventService.get(id=event_id)
    audit_root = os.path.join(MEDIA_ROOT, 'event/' + str(event_id) + '/audit.csv')
    figure_root = os.path.join(MEDIA_ROOT, 'event/' + str(event_id) + '/report_plot.png')

    context = {}

    if request.POST.has_key('generate'):
        figure_path = EventService.generate_report_image(int(event_id))
        e.figure_path = figure_path
        e = EventService.set_figure_path(e.id, figure_path)
        error_message = 'Gráfica generada con éxito.'
        context['error_message'] = error_message

    if request.POST.has_key('save'):
        scheduler.write_event(event_id)
        error_message = 'Se añade este evento al modelo.'
        context['error_message'] = error_message


    context['e'] = e

    if os.path.exists(audit_root) and not os.path.exists(figure_root):
        context['audit_root'] = audit_root


    return render(request, 'boiler_manager/event/event_detail.html', context)
