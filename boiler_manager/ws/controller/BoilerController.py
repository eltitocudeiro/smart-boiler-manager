# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import OperationalError
from django.shortcuts import render
from boiler_manager.model.service import BoilerService as BoilerService
from boiler_manager.ws.mapper import RequestMapper

from smart_boiler_manager.settings import BOILER_TYPE_LIST


def boiler(request):
    boiler_type_list = BOILER_TYPE_LIST
    try:
        if request.POST.has_key('brand') and request.POST.has_key('model') \
                and request.POST.has_key('power') and request.POST.has_key('max_temp') \
                and request.POST.has_key('min_temp') and request.POST.has_key('type'):
            b = BoilerService.create(RequestMapper.to_boiler(request))
            context = {'b': b}
            return render(request, 'boiler_manager/boiler/boiler_detail.html', context)
        context = {'boiler_type_list': boiler_type_list}
    except ValueError:
        error_message = 'Datos de entrada no válidos'
        context = {'error_message': error_message, 'boiler_type_list': boiler_type_list}
    return render(request, 'boiler_manager/boiler/boiler.html', context)


def boilers(request):
    try:
        if request.user.is_staff:
            boiler_list = BoilerService.get_all()
            context = {'boiler_list': boiler_list}
        else:
            context = {}
    except OperationalError:
        context = {}
    return render(request, 'boiler_manager/boiler/boilers.html', context)


def boiler_detail(request, boiler_id):
    try:
        b = BoilerService.get(boiler_id)
        context = {'b': b}
    except OperationalError:
        context = {}
    return render(request, 'boiler_manager/boiler/boiler_detail.html', context)
