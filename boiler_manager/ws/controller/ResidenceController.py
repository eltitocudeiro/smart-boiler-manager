# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import OperationalError
from boiler_manager.model.repository.entity import Residence
from boiler_manager.model.service import ResidenceService, BoilerService, UserService, GoogleMapsService, GoogleCalendarService
from django.shortcuts import render
from boiler_manager.ws.mapper import RequestMapper
from smart_boiler_manager.settings import GOOGLE_API_KEY


def residence(request):
    try:
        boiler_list = BoilerService.get_all()
        if request.POST.has_key('title') and request.POST.has_key('boiler'):
            title = str(RequestMapper.get_field(request, 'title'))
            x_side = float(RequestMapper.get_field(request, 'x_side'))
            y_side = float(RequestMapper.get_field(request, 'y_side'))
            boiler_id = int(RequestMapper.get_field(request, 'boiler'))
            b = BoilerService.get(boiler_id)
            r = ResidenceService.create(title=title, address=None, google_calendar=None, boiler=b, user=request.user,
                                        x_side=x_side, y_side=y_side)
            context = {'r': r}
            return render(request, 'boiler_manager/residence/residence_detail.html', context)
        context = {'boiler_list': boiler_list}
    except (TypeError, ValueError):
        error_message = 'Datos de entrada no válidos'
        context = {'error_message': error_message, 'boiler_list': boiler_list}
    return render(request, 'boiler_manager/residence/residence.html', context)


def residence_detail(request, home_id):
    try:
        r = ResidenceService.get(home_id)
        context = {}

        if request.POST.has_key('calendar'):
            try:
                calendar = GoogleCalendarService.getCalendar(r.google_calendar)
                calendar_id = calendar['id']
                error_message = 'Este calendario ya existe.'
                context['error_message'] = error_message
            except:
                calendar_name = 'BoilerManager - Residence:' + str(r.id)
                calendar = GoogleCalendarService.createCalendar(calendar_name)
                calendar_id = calendar['id']
                email = str(request.user.email)
                try:
                    changed_rule = GoogleCalendarService.setAcl(calendar['id'], email, "writer")
                except:
                    print('<HttpError 403 when requesting https://www.googleapis.com/calendar/v3/calendars/ returned: '
                          '"Cannot change your own access level.">')
                    # error_message = 'En esta aplicación no puedes cambiar tu propio nivel de acceso.'
                    # context['error_message'] = error_message
            r = ResidenceService.set_google_calendar(residence=r, google_calendar=calendar_id)
        context['r'] = r
    except Residence.DoesNotExist:
        context = {}
    return render(request, 'boiler_manager/residence/residence_detail.html', context)


def set_address(request, home_id):
    try:
        r = ResidenceService.get(home_id)
        if request.POST.has_key('lat') and request.POST.has_key('lon'):
            address = GoogleMapsService.InverseGeocoding(
                    latitud=RequestMapper.get_field(field='lat', request=request),
                    longitud=RequestMapper.get_field(field='lon', request=request))
            r = ResidenceService.set_address(address=address, residence=r)
            context = {'r': r}
            return render(request, 'boiler_manager/residence/residence_detail.html', context)
        context = {'google_key': GOOGLE_API_KEY, 'r': r}
    except Residence.DoesNotExist:
        error_message = 'Residencia no encontrada'
        context = {'error_message': error_message}
    except IndexError:
        error_message = 'Coordenadas no establecidas'
        context = {'error_message': error_message, 'google_key': GOOGLE_API_KEY, 'r': r }
    return render(request, 'boiler_manager/residence/set_address.html', context)


def residences(request):
    try:

        if request.user.is_staff:
            residence_list = ResidenceService.get_all()
            context = {'residence_list': residence_list}
        elif request.user.is_authenticated:
            residence_list = ResidenceService.get_all_by_user(user=request.user)
            context = {'residence_list': residence_list}
        else:
            context = {}
    except OperationalError:
        context = {}
    return render(request, 'boiler_manager/residence/residences.html', context)


def residence_by_user(request, user_id):
    try:
        user = UserService.get_by_id(id=user_id)
        residence_list = ResidenceService.get_all_by_user(user=user)
        if residence_list:
            context = {'residence_list': residence_list}
        else:
            context = {}
    except User.DoesNotExist:
        context = {}
    return render(request, 'boiler_manager/residence/residence_by_user.html', context)


