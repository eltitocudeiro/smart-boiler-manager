# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponseRedirect
from django.shortcuts import render

from boiler_manager.model.service import ResidenceService, FirebaseService, GoogleMapsService
from boiler_manager.ws.mapper import RequestMapper
from boiler_manager.ws.util.forms import UserCreationForm
from smart_boiler_manager.settings import GOOGLE_API_KEY


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            return HttpResponseRedirect("/boiler-manager/")
    else:
        form = UserCreationForm()
    context = {'form': form, }
    return render(request, "registration/register.html", context)


def geolocation(request):
    context = {'google_key': GOOGLE_API_KEY}

    # if request.user.is_authenticated():
    #     try :
    #         lat_user, lng_user = FirebaseService.get_user_position(request.user.username)
    #         user_position = GoogleMapsService.InverseGeocoding(latitud=lat_user, longitud=lng_user)
    #         context['lat_user'] = str(lat_user)
    #         context['lng_user'] = str(lng_user)
    #         context['user_position'] = user_position
    #     except AttributeError:
    #         error_message = 'El usuario no tiene la localización realtime activada.'
    #         context['error_message'] = error_message

    if request.POST.has_key('res'):
        try:
            r = ResidenceService.get(int(RequestMapper.get_field(request, 'res')))
            context['r'] = r
            lat_home, lng_home = GoogleMapsService.Geocoding(r.address)
            context['lat_home'] = str(lat_home)
            context['lng_home'] = str(lng_home)
        except ValueError:
            error_message = 'Escoge una residencia.'
            context['error_message'] = error_message
            if request.user.is_staff:
                residences = ResidenceService.get_all()
            else:
                residences = ResidenceService.get_residence_list_with_address(request.user)
            context['residences'] = residences
    else:
        if request.user.is_staff:
            residences = ResidenceService.get_all()
        elif request.user.is_authenticated():
            residences = ResidenceService.get_residence_list_with_address(request.user)
        else:
            residences = []

        if len(residences) == 0:
            if request.user.is_staff:
                error_message = 'No hay residencias registradas en el sistema.'
            else:
                error_message = 'Este usuario no tiene ninguna residencia con una dirección registrada.'
            context['error_message'] = error_message
        elif len(residences) == 1:
            r = residences.pop()
            context['r'] = r
            lat_home, lng_home = GoogleMapsService.Geocoding(r.address)
            context['lat_home'] = str(lat_home)
            context['lng_home'] = str(lng_home)
        else:
            error_message = 'Más de una residencia registrada. Por favor, elige una para continuar.'
            context['residences'] = residences
            context['error_message'] = error_message

    try:
        if r:
            if request.user.is_staff:
                username = r.user.all()[0].username
            else:
                username = request.user.username
            context['username'] = username

            lat_user, lng_user = FirebaseService.get_user_position(username)
            user_position = GoogleMapsService.InverseGeocoding(latitud=lat_user, longitud=lng_user)
            context['lat_user'] = str(lat_user)
            context['lng_user'] = str(lng_user)
            context['user_position'] = user_position

        if r.address and user_position:
            travel_dist_car, travel_time_car = GoogleMapsService.DistanceMatrix(user_position, r.address)
            travel_dist_walk, travel_time_walk = GoogleMapsService.DistanceMatrix(user_position, r.address, 'walking')
            travel_dist_train, travel_time_train = GoogleMapsService.DistanceMatrix(user_position, r.address, 'train')
            travel_dist_bike, travel_time_bike = GoogleMapsService.DistanceMatrix(user_position, r.address, 'bicycling')
            context['travel_dist_car'] = travel_dist_car
            context['travel_time_car'] = travel_time_car
            context['travel_dist_walk'] = travel_dist_walk
            context['travel_time_walk'] = travel_time_walk
            context['travel_dist_train'] = travel_dist_train
            context['travel_time_train'] = travel_time_train
            context['travel_dist_bike'] = travel_dist_bike
            context['travel_time_bike'] = travel_time_bike

    except AttributeError:
        error_message = 'El usuario no tiene la localización realtime activada.'
        context['error_message'] = error_message
    except UnboundLocalError:
        pass

    return render(request, "boiler_manager/user_geolocation.html", context)
