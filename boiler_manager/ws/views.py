# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from django.shortcuts import render
from django.http import HttpResponseRedirect
# from boiler_manager.util.forms import UserCreationForm
# import boiler_manager.service.boiler.service as BoilerService
# import boiler_manager.service.residence.service as ResidenceService
# import boiler_manager.service.user.service as UserService
# import boiler_manager.service.event.service as EventService
# import boiler_manager.service.api.owm.weather_service as TempService
# import boiler_manager.service.util.scheduler as Scheduler

from smart_boiler_manager.settings import GOOGLE_API_KEY as GOOGLE_KEY, MEDIA_URL, MEDIA_ROOT, BASE_DIR, \
    BOILER_TYPE_LIST


# Create your views here.

def index(request):
    # context = {}
    # return render(request, 'boiler_manager/index.html', context)
	pass


def boiler(request):
    # if request.POST.has_key('brand') and request.POST.has_key('model') \
            # and request.POST.has_key('power') and request.POST.has_key('max_temp') \
            # and request.POST.has_key('min_temp') and request.POST.has_key('type'):
        # BoilerService.create(request)
    # boiler_list = BoilerService.get_all()
    # boiler_type_list = BOILER_TYPE_LIST
    # context = {'boiler_list': boiler_list, 'boiler_type_list': boiler_type_list}
    # return render(request, 'boiler_manager/boiler.html', context)
	pass
	

def boilers(request):
    # boiler_list = BoilerService.get_all()
    # context = {'boiler_list': boiler_list}
    # return render(request, 'boiler_manager/boilers.html', context)
	pass
	

def boiler_detail(request, boiler_id):
    # b = BoilerService.get(boiler_id)
    # context = {'b': b}
    # return render(request, 'boiler_manager/boiler_detail.html', context)
	pass
	

def residence(request):
    # if request.POST.has_key('title') and request.POST.has_key('boiler'):
        # ResidenceService.create(request)
    # residence_list = ResidenceService.get_all()
    # boiler_list = BoilerService.get_all()
    # context = {'residence_list': residence_list, 'boiler_list': boiler_list}
    # return render(request, 'boiler_manager/residence.html', context)
	pass
	

def residence_detail(request, home_id):
    # r = ResidenceService.get(home_id)
    # context = {'r': r}
    # return render(request, 'boiler_manager/residence_detail.html', context)
	pass
	

def set_address(request, home_id):
    # r = ResidenceService.get(home_id)
    # if request.POST.has_key('lat') and request.POST.has_key('lon'):
        # r = ResidenceService.set_address(request, r)
    # context = {'r': r, 'google_key': GOOGLE_KEY}
    # return render(request, 'boiler_manager/set_address.html', context)
	pass
	

def set_google_calendar(request, home_id):
    # r = ResidenceService.get(home_id)
    # if request.POST.has_key('fecha-inicial'):
        # r = ResidenceService.set_google_calendar(request, r)
    # context = {'r': r}
    # return render(request, 'boiler_manager/set_google_calendar.html', context)
	pass

	
def scheduler(request):
    # if request.user.is_authenticated():
        # u = UserService.get_user_s(user_m=request.user)
        # if request.POST.has_key('latitud') and request.POST.has_key('longitud'):
            # u = UserService.set_position(request)
        # context = {'u': u, 'google_key': GOOGLE_KEY}
    # else:
        # context = {'google_key': GOOGLE_KEY}
    # if request.POST.has_key('begin'):
        # event_list = Scheduler.bootScheduler(None)
        # # context={'event_list':event_list}
    # return render(request, 'boiler_manager/scheduler.html', context)
	pass
	

def residences(request):
    # residence_list = ResidenceService.get_all()
    # context = {'residence_list': residence_list}
    # return render(request, 'boiler_manager/residences.html', context)
	pass
	
	
def residence_by_user(request, user_id):
    # try:
        # user = UserService.get_user_m(id=user_id)
        # residence_list = ResidenceService.get_all_by_user(user=user)
        # if residence_list:
            # context = {'residence_list': residence_list}
        # else:
            # context = {}
    # except:
        # context = {}
    # return render(request, 'boiler_manager/residence_by_user.html', context)
	pass
	

def event(request):
    # if request.POST.has_key('fecha-inicial'):
        # EventService.create(request)
    # residence_list = ResidenceService.get_valid_user_residence_list(request.user)
    # context = {'residence_list': residence_list}
    # return render(request, 'boiler_manager/event.html', context)
	pass
	

def events(request):
    # event_list = EventService.get_all()
    # context = {'event_list': event_list}
    # return render(request, 'boiler_manager/events.html', context)
	pass
	

def event_detail(request, event_id):
    # e = EventService.get(id=event_id)
    # audit_root = os.path.join(MEDIA_ROOT, 'event/' + str(event_id) + '/audit.csv')
    # figure_root = os.path.join(MEDIA_ROOT, 'event/' + str(event_id) + '/report_plot.png')
    # print(audit_root)

    # if request.POST.has_key('generate'):
        # figure_path = EventService.generate_report_image(int(event_id))
        # e.figure_path = figure_path
        # e = EventService.set_figure_path(e.id, figure_path)

    # if os.path.exists(audit_root) and not os.path.exists(figure_root):
        # context = {'e': e, 'audit_root': audit_root}
    # else:
        # context = {'e': e}

    # return render(request, 'boiler_manager/event_detail.html', context)
	pass
	

def register(request):
    # if request.method == 'POST':
        # form = UserCreationForm(request.POST)
        # if form.is_valid():
            # new_user = form.save()
            # return HttpResponseRedirect("/boiler-manager/")
    # else:
        # form = UserCreationForm()
    # context = {'form': form, }
    # return render(request, "registration/register.html", context)
	pass
	