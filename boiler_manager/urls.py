"""smart_boiler_manager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from boiler_manager.ws.controller import AppController, BoilerController, ResidenceController, EventController, \
    UserController, AdminController
from boiler_manager.scheduler import scheduler

urlpatterns = [
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^register/', UserController.register, name='register'),
    url(r'^$', AppController.index, name='index'),
    url(r'^boiler/$', BoilerController.boiler, name='boiler'),
    url(r'^boilers/$', BoilerController.boilers, name='boilers'),
    url(r'^boiler/(?P<boiler_id>\d+)/$', BoilerController.boiler_detail, name='boiler_detail'),
    url(r'^residence/$', ResidenceController.residence, name='residence'),
    url(r'^residence/(?P<home_id>\d+)/$', ResidenceController.residence_detail, name='residence_detail'),
    url(r'^residence/(?P<home_id>\d+)/address/$', ResidenceController.set_address, name='set_address'),
    url(r'^residences/$', ResidenceController.residences, name='residences'),
    url(r'^residences/(?P<user_id>\d+)/$', ResidenceController.residence_by_user, name='residence_by_user'),
    url(r'^event/$', EventController.event, name='event'),
    url(r'^events/$', EventController.events, name='events'),
    url(r'^event/(?P<event_id>\d+)/$', EventController.event_detail, name='event_detail'),
    url(r'^scheduler/$', AdminController.scheduler, name='scheduler'),
    url(r'^geolocation/$', UserController.geolocation, name='geolocation'),
]


scheduler.bootScheduler(None)